from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [

    # django-jet

	url(r'^jet/', include('jet.urls', 'jet')),

	# ck editor
	
	url(r'^ckeditor/', include('ckeditor_uploader.urls')),

	# admin
    
    url(r'^admin/', include(admin.site.urls)),
]


from django.conf import settings
from django.conf.urls.static import static
if settings.DEBUG:
    urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + urlpatterns