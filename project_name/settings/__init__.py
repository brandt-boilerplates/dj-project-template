#__init__.py for our settings module
import os
import decouple as config

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  
from base import * # required because that is what we will override

# now first import dev
if config('ENV_TYPE') == 'DEV':
    from dev import *
else:
	from prod import *