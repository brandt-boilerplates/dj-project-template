from .base import *

DEBUG = False

# html minify 

HTML_MINIFY = True
EXCLUDE_FROM_MINIFYING = ('^admin/')

ALLOWED_HOSTS = ['{{ project_name }}.com.br', 'www.{{ project_name }}.com.br', ]

DATA_UPLOAD_MAX_NUMBER_FIELDS = None

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASSWORD'),
        'HOST': config('DB_HOST'),
    }
}

NOCAPTCHA = True
