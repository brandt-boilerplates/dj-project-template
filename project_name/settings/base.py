# -*- coding: utf-8 -*-
import os

from decouple import config
from django.conf.locale.pt_BR import formats as pt_BR_formats

pt_BR_formats.DATETIME_FORMAT = "d/m/y (H:i)"

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '{{ secret_key }}'

# email back-end

# EMAIL_USE_TLS = True
# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_HOST_USER = ''
# EMAIL_HOST_PASSWORD = config('EMAIL_PASSWORD')
# EMAIL_PORT = 587

INSTALLED_APPS = (
    
    '{{ project_name }}',
    'jet',

    # Django

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    # third-parties

    'ckeditor',
    'ckeditor_uploader',
    'el_pagination',
    'captcha',


)

SITE_ID = 1

# EL PAGINATION

EL_PAGINATION_PER_PAGE = 6
EL_PAGINATION_PREVIOUS_LABEL = 'Posts mais recentes'
EL_PAGINATION_NEXT_LABEL = 'Posts mais antigos'

# CK EDITOR

CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_IMAGE_BACKEND = 'Pillow'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware',
)

# django-jet options

JET_DEFAULT_THEME = 'light-gray'

JET_SIDE_MENU_COMPACT = True

JET_SIDE_MENU_CUSTOM_APPS = [
    ('auth', [ 
        'User', 'Group',
    ]),
	 ('cadastros', [ 
        'Product', 'Timeline', 'Video',
    ]),
    ('blog', [ 
        'Article', 'Category', 'Tag',
    ]),
]


ROOT_URLCONF = '{{ project_name }}.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.i18n',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

WSGI_APPLICATION = '{{ project_name }}.wsgi.application'


LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Fortaleza'

USE_L10N = True

USE_TZ = False

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join('{{ project_name }}', 'static-root')


STATICFILES_DIRS = (
    os.path.join('{{ project_name }}', 'static'),
)

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join('{{ project_name }}', "media")
