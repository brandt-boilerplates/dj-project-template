from .base import *

ALLOWED_HOSTS = ['*']

DEBUG = True

# html minify 

HTML_MINIFY = False
EXCLUDE_FROM_MINIFYING = ('^admin/')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join('db.sqlite3'),
    }
}



NOCAPTCHA = True
RECAPTCHA_USE_SSL = False